class camper():
	def __init__(self, name, batch, course_type):
		self.name = name
		self.batch = batch
		self.course_type = course_type 
	def career_track(self):
		print(f"Currently enrolled in {self.course_type} program")
	def info(self):
		print(f"My name is {self.name} of batch {self.batch}")

zuitt_camper = camper("Alan","100", "python short course" )

print(f'Camper name: {zuitt_camper.name}')
print(f'Camper batch: {zuitt_camper.batch}')
print(f'Camper course: {zuitt_camper.course_type}')
zuitt_camper.career_track()
zuitt_camper.info()

